$(function () {
    var APPLICATION_ID = "A043E307-D4F1-984F-FFA4-5C0C7EBDFC00",
    SECRET_KEY = "89A6DE7B-61DF-DFC7-FFF0-967381BE1900",
    VERSION = "v1";
  
  Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
  
  var postsCollection = Backendless.Persistence.of(Posts).find();
  
  console.log(postsCollection);
  
  var wrapper = {
      posts: postsCollection.data 
  };
  
  
  Handlebars.registerHelper('format', function (time) {
      return moment (time).format("dddd, MMMM Do YYYY");
      
  })
  
  var blogScript = $("#blogs-template").html();
  var blogTemplate = Handlebars.compile(blogScript);
  var blogHTML = blogTemplate(wrapper);
  
  $('.main-container').html(blogHTML);
  
});

function Posts (args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}
